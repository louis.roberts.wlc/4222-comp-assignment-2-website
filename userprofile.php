<?php

session_start();

include("api/api.inc.php");

$formdata = formProcess($_REQUEST) ?? array();

//get the id of the user.
$userid = $_SESSION["usrID"] ?? -1;
$userid -= 1;

if (is_numeric($userid) && $userid >= 0)
{

    $fulluserdata = file('data/json/accdetails.json');
    
    $tempuser = $fulluserdata[$userid];
    
    $user = json_decode($tempuser);

    if ($user->favedeviceid == 0)
    {   //Placeholder text if the user hasn't picked a faveourite device yet.
        $device = "None! Edit your profile to add a favourite device!";
    }
    else
    {   //Get the name of the users favourite device
        $deviceobj = jsonLoadSingleDevice($user->favedeviceid);
        $device = $deviceobj->devicename;
    }
    
}

if($_SERVER["REQUEST_METHOD"] == "POST")
{
    (int)$favedevice = $_REQUEST["deviceID"];
    //TODO: Make the edit user data form work
    $newuserdata = new bllAccount();
    $newuserdata->firstname = appFormProcessData($_SESSION["usrFirstName"] ?? "");
    $newuserdata->lastname = appFormProcessData($_SESSION["usrLastName"] ?? "");
    $newuserdata->emailaddress = appFormProcessData($_SESSION["usrEmail"] ?? "");
    $newuserdata->favedeviceid = (int)$favedevice;
    
    $isformvalid = true;
    //Check to see if any form data is missing.
    if($newuserdata->firstname == "")
    {
        $isformvalid = false;
    }
    if($newuserdata->lastname == "")
    {
        $isformvalid = false;
    }
    if($newuserdata->emailaddress == "")
    {
        $isformvalid = false;
    }
    if($newuserdata->favedeviceid == "")
    {
        $isformvalid = false;
    }
    //If any data is missing send the user to the error page
    if($isformvalid == false)
    {
        appGoToError();
    }
    else //Edit the user's entry in the JSON file
    { 
        //Open the file with the user details
        $userfile = file('data/json/accdetails.json');
        //Pull the line containing the data for the currently logged in user.
        $tempuserdata = $userfile[$userid];
        //Decode into an stdobj
        $olduserjson = json_decode($tempuserdata);
        //Change the data
        $olduserjson->favedeviceid = $newuserdata->favedeviceid;
        $newuserjson = json_encode($olduserjson);
        $userfile[$userid] = $newuserjson;
        $newline = "";
        
        //This creates an extra blank line at the end of the JSON file that we need to remove.
        //Otherwise things will eventually break if it's not removed.
        foreach($userfile as $userline)
        {
            $newline .= $userline.PHP_EOL;
        }
        
        //Save the modified data
        file_put_contents("data/json/accdetails.json",$newline);
        
        //These three lines remove the extra blank line.
        $file = file("data/json/accdetails.json");
        array_pop($file);
        file_put_contents("data/json/accdetails.json", $file);
        
        //Then this is used to notify the user that everything worked.
        $_SESSION["isUpdateSuccessful"] = true;
        
    }
}

function formProcess(array $formdata): array
{
    
    foreach ($formdata as $field => $value)
    {
        $formdata[$field] = appFormProcessData($value);
    }
    $isformvalid = true;
    if ($isformvalid && empty($formdata["reviewcontent"]))
    {
        $isformvalid = false;
    }
    if ($isformvalid && empty($formdata["score"]))
    {
    }
    if($isformvalid)
    {
        $formdata["valid"] = true;
    }
    return $formdata;
}

function createPage($user, $device, $formdata, $update)
{
    $formmethod = "POST";
    $formaction = htmlspecialchars($_SERVER['PHP_SELF']);
    
    /* usr profile structure
     * id
     * favedeviceid
     * firstname
     * lastname
     * password
     * emailaddress
     */
        $content = <<<USR

	<div class="row container-fluid">
		<div class="col-md text-center mb-2">
			<h2>User Profile - {$user->firstname} {$user->lastname}</h2>
            {$update}
		</div>
        <div class="container-fluid">
            <h3> Details </h3>
                <ul class="list-group">
                  <li class="list-group-item">First Name: {$user->firstname}</li>
                  <li class="list-group-item">Last Name: {$user->lastname}</li>
                  <li class="list-group-item">Email Address: {$user->emailaddress}</li>
                  <li class="list-group-item">Favourite Device: {$device} </li>
                </ul>
        </div>
    </div>
    <div class="accordion" id="usrEdit">
    		<div class="accordion-item">
    			<h2 class="accordion-header" id="usrEditHeader">
    				<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#usrEditCollapse" aria-expanded="false" aria-controls="usrEditCollapse">
     					Select Favorite Device
    				</button>
    			</h2>
    			<div id="usrEditCollapse" class="accordion-collapse collapse" aria-labelledby="usrEditHeader" data-bs-parent="usrEdit">
                    <div id="signupform" class="container">
                        <form id="usrEdit" name="usrEdit" action="{$formaction}" method="{$formmethod}" role="form" class="navbar-form" onsubmit="validateSignUpForm()">
                        <div class="mb-2 form-group pt-3">
                            <label for="deviceID">Choose your favourite device</label>
                            <select name="deviceID" id="deviceID">
                              <option value=1>iPhone 14</option>
                              <option value=2>iPhone 13</option>
                              <option value=3>iPad (9th Gen)</option>
                              <option value=4>iPad (10th Gen)</option>
                            </select> 
                        </div>
                            <button type="submit" class="mb-2 btn btn-primary">Set favorite Device </button>
                        </form>
                    </div>
    			</div>
            </div>
            </div>

USR;
        return $content;
}

if(isset($_SESSION["usrID"]))
{
    if(isset($_SESSION["isUpdateSuccessful"]))
    {
        $update = <<<UPDATE
<h3 style="color:#ff0000";>Profile Update Successful!</h3>
UPDATE;
        unset($_SESSION["isUpdateSuccessful"]);
    }
    else 
    {
        $update="";
    }
    $pagecontent = createPage($user, $device, $formdata, $update);
    $tabtitle = "Device Page";
    
    //build html
    
    $page = new MasterPage($tabtitle);
    $page->setDynamicContent2($pagecontent);
    $page->renderPage();
}
else
{
    appGoToError();
}

?>
