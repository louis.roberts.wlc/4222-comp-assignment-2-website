<?php 

//Creates JSON templates for each class

require_once("api/api.inc.php");

function jsonCreateAccDetails($file)
{
    $account = new bllAccount();
    $account->id = 0;
    $account->favedeviceid = 0;
    $account->firstname = "";
    $account->lastname = "";
    $account->password = "";
    $account->emailaddress = "";
    $data = json_encode($account).PHP_EOL;
    file_put_contents($file,$data);
	return $data;
}

function jsonCreateDevice($file)
{
    $device = new bllDevice();
    $device->id = 0;
    $device->devicename = "";
    $device->manufacturername = "";
    $device->batterysize = "";
    $device->releasedate = "";
    $device->osversion = "";
    $device->processorname = "";
    $device->corecount = "";
    $device->coredetails = "";
    $data = json_encode($device).PHP_EOL;
    file_put_contents($file,$data);
	return $data;
}



function jsonCreateExtReview($file)
{
    $extreview = new bllExtReview();
    $extreview->id = 0;
    $extreview->deviceid = 0; 
    $extreview->source = "";
    $extreview->sitelink = "";
    $extreview->type = "";
    $data = json_encode($extreview).PHP_EOL;
    file_put_contents($file,$data);
	return $data;
}

function jsonCreateRetailer($pfile)
{
    $retailer = new bllretailer();
    $retailer->id = 0;
    $retailer->deviceid = 0;
    $retailer->retailer = "";
    $retailer->price = "";
    $retailer->sitelink = "";
    $data = json_encode($retailer).PHP_EOL;
    file_put_contents($pfile,$data);
	return $data;
}

function jsonCreateUsrReview($pfile)
{
    $ursreview = new bllUsrReview();
    $ursreview->id = 0;
    $ursreview->deviceid = 0;
    $ursreview->firstname = "";
    $ursreview->lastname = "";
    $ursreview->score = 0;
    $ursreview->reviewtext = "";
    $data = json_encode($ursreview).PHP_EOL;
    file_put_contents($pfile,$data);
	return $data;
}



//---------Create JSON Files---------------------------------------------
//UNCOMMENT TO CREATE A NEW FILE
// jsonCreateAccDetails("data/json/accdetails.json");
// jsonCreateDevice("data/json/devices.json");
// jsonCreateExtReview("data/json/extreview.json");
// jsonCreateRetailer("data/json/retailer.json");
// jsonCreateUsrReview("data/json/retailer.json");


?>