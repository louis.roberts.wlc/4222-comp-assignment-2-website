<?php

session_start();

include("api/api.inc.php");

#page generation
function createPage($tablecontent)
{
    $content = <<<PAGE
<div class="container">
        <ul class="nav nav-pills">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Sort by</a>
            <div class="dropdown-menu">
              <button class="dropdown-item" onclick="sortTable()">Sort by device type</button>
              <button class="dropdown-item" onclick="testButton()">Sort by Battery Capacity (Ascending)</button>
              <button class="dropdown-item" onclick="testButton()">Sort by Battery Capacity (Descending)</button>
              <button class="dropdown-item" onclick="testButton()">Sort by TDS Score (Ascending)</button>
              <button class="dropdown-item" onclick="testButton()">Sort by TDS Score (Descending)</button>
              <button class="dropdown-item" onclick="testButton()">Randomise list</button>
            </div>
        </ul>
        <table class="table table-striped table-hover" id="devicetable">
            <thead>
                <tr>
                    <th scope="col">Device</th>
                    <th scope="col">Device Type</th>
                    <th scope="col">Manufacturer</th>
                    <th scope="col">Battery Capacity</th>
                    <th scope="col">OS</th>
                    <th scope="col">Processor</th>
                    <th scope="col">Core Count</th>
                    <th scope="col">Core Details</th>
                    <th scope="col">Release Date</th>
                    <th scope="col">Overall TDR Score</th>
                </tr>
            </thead>
                <tbody>
                        {$tablecontent}
                </tbody>
        </table>
    </div>
<div>
PAGE;    
    return $content;
}


$devicearray = [];
$devicearray[] = jsonLoadAllDevices();
$tablebody = "";

//generate the table

foreach($devicearray as $devices)
{
    foreach($devices as $device)
    {      
        $tablebody .= renderDeviceTable($device);
    }
}


$pagecontent = createPage($tablebody);
$tabtitle = "Device Page";

//build html
$page = new MasterPage($tabtitle);
$page->setDynamicContent2($pagecontent);
$page->renderPage();

?>