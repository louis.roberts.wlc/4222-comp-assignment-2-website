<?php
//----INCLUDE APIS------------------------------------
include("api/api.inc.php");

//----PAGE GENERATION LOGIC---------------------------
session_start();

function createPage($error = "")
{
    
    if($error = "")
    {
        $errormsg = <<<ERR
    <h3> We could not determine the cause of the error <h3>
ERR;
    }
    else 
    {
        $errormsg = <<<ERR
    <h3> The following error message has been 
ERR;
    }
    
    $tcontent = <<<PAGE
 <h1>An error has occured...</h1>
 <h2>Click the button below to return to the home page<h2>
 <p><a href="index.php" class="btn btn-primary">Go Home</a></p>
PAGE;
    return $tcontent;
}

//Build up our Dynamic Content Items.

$error = isset($_SESSION["errmsg"]) ? $_SESSION["errmsg"] : "";
unset($_SESSION["errmsg"]);
$tpagetitle = "Error";
$tpagecontent = createPage($error);

//----BUILD OUR HTML PAGE----------------------------
//Create an instance of our Page class
$tpage = new MasterPage($tpagetitle);
$tpage->setDynamicContent2($tpagecontent);
$tpage->renderPage();
?>