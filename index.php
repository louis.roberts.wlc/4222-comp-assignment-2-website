<?php

session_start();

include ("api/api.inc.php");

// page generation
function createPage($pshowcase, $particles, $username = "")
{

    // PHP complains unless this is done
    $articles = null;
    foreach ($particles as $particle)
    {
        foreach ($particle as $item)
        {
            $articles .= renderArticlePreview($item);
        }
    }

    $content = <<<PAGE
        {$pshowcase}
        {$articles}
    
    PAGE;
    return $content;
}

$showcase = renderShowcasePreview();

$articles[] = jsonLoadAllArticlePreviews();

// check to see if the article previews loaded properly
if (count($articles) === 0)
{
    appGoToError();
}
else // Previews loaded properly
{
    $pagecontent = createPage($showcase, $articles);
    $tabtitle = "Device Page";

    // build html
    $page = new MasterPage($tabtitle);
    $page->setDynamicContent2($pagecontent);
    $page->renderPage();
}

?>
