<?php

session_start();

include ("api/api.inc.php");

// page generation
function createPage($email)
{
    $content = <<<PAGE

<div class="container d-flex justify-content-center">
    <div class="text-center">
        <h3 class="text-center"> Successfully logged in as: {$email}!</h3>
        <p class="lead fs-4 fw-bold"> You will be redirected in 5 seconds. </p>
        <a href="index.php" class="fw-light"> Click here to manually redirect </a>
    </div>
</div>

PAGE;

    return $content;

}

//Check to see if the user has logged in before reaching this page.
if ((isset($_SESSION["usrLoggedIn"])) && ($_SESSION["usrLoggedIn"] == True))
{
    $email = $_SESSION["usrEmail"];
    $pagecontent = createPage($email);
    $tabtitle = "Sign Up Successful";

    // build html
    $page = new MasterPage($tabtitle);
    $page->setDynamicContent2($pagecontent);
    $page->renderPage();
    header('Refresh: 5; URL=index.php');
}
else //User shouldn't be on this page if they havent gone through the login page AND successfully logged in. Boot them to an error.
{
    appGoToError();
}

?>
