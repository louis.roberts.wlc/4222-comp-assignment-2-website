<?php

include("api/api.inc.php");

session_start();

$myname = $_REQUEST["myname"] ?? "";

$token = $_SESSION["username"] ?? "";

if(empty($token) && !empty($myname))
{
    $_SESSION["myuser"] = processRequest("$myname");
    $_SESSION["entered"] = true;
    appGoToHome();
}
else 
{
    appGoToError();
}

?>