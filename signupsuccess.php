<?php

session_start();

include ("api/api.inc.php");

// page generation
function createPage($email)
{
    $content = <<<PAGE

<div class="container d-flex justify-content-center">
    <div class="text-center">
        <h3 class="text-center"> Account: {$email} has been successfully created!</h3>
        <p class="lead fs-4 fw-bold"> Thank you for creating an account with The Device review! </p>
        <p> You will be redirected in 15 seconds.</p>
        <a href="index.php" class="fw-light"> Click here to manually redirect </a>
    </div>
</div>

PAGE;

    return $content;

}


if (!isset($_SESSION["newuseremail"]))
{
    appGoToError();
}
else // Previews loaded properly
{
    $email = $_SESSION["newuseremail"];
    $pagecontent = createPage($email);
    $tabtitle = "Sign Up Successful";

    // build html
    $page = new MasterPage($tabtitle);
    $page->setDynamicContent2($pagecontent);
    $page->renderPage();
    
    // Clear the session variable that holds the new account email as it's no longer needed.
    unset($_SESSION["newuseremail"]);
    header('Refresh: 10; URL=index.php');
}

?>