<?php

session_start();

include ("api/api.inc.php");

// page generation
function createPage($pshowcase, $particles, $username = "")
{
    $imagedir = "img/OS/";
    $ioslogo = "IOS_logo.svg";
    $appstore = "appstore.png";
    $widget = "widget.png";
    $deptheffect = "deptheffect.png";
    $imsg = "imsg.png";
    
    
    $content = <<<PAGE
    <div class="row container-fluid">
        <div class="col-md text-center mb-2">
            <h2>iOS Showcase</h2>			
        </div>
        <div class="d-flex justify-content-center">
            <img src="{$imagedir}{$ioslogo}" class="image-flex mt-2" style="width: 200px; height: 200px" alt="iOS Logo">
        </div>
        <div class="d-flex justify-content-center">
            <article class="text-justify justify-content-center">
                <h2 class="text-center mt-2 underline"> Introduction </h2>
                <p> 
                    OS is a Mobile Operating System (or OS) developed by Apple for their iPhone line of Smartphones and is the second most used Mobile OS used in the world behind Google's Android OS.
                    iOS has been around since 2007 and since then many versions of iOS have come and gone bringing with them many new features and improvements.
                    The latest major version of iOS is iOS 16 having been released in 2022, being only a minor upgrade when compared to the previous major version of iOS 15 that was released in 2021.
                    This article will cover some important information about iOS 16 while comparing it to iOS 15.
                </p>
                <p>
                    A basic overview in how iOS 16 works can be seen in the video below by the Youtube Channel: Simple Alpaca 
                </p>
                <div class="youtube">
                <iframe style="display: block; margin: auto;" width="640" height="360" src="https://www.youtube.com/embed/D9RWFQ2xtys" title="How To Use iOS 16! (Complete Beginners Guide)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                </div>
                
                <h2 class="text-center mt-4 underline"> iOS Availability </h2>
                <p>
                    iOS 16 is only available on Apple's iPhone line up from the iPhone 8 onwards. With prior iphones being locked to use previous versions of the OS.
                    iOS 15, on the other hand, is avilable on devices starting from the iPhone 6s onwards. However due to system updates, it is more likely that iPhones from 8 onwards will be using iOS 16.
                </p>
                
                <h2 class="text-center mt-4 underline"> App Availability </h2>
                <p>
                    There are more than 1.8 million apps available to use on iOS 16, all available through the built in App Store.
                    To install an app, a user can simply open the app store and they will be presented with a selection of apps to choose from.
                    Users can also search for apps using the built in search function.
                    Once a user has found an app they want to download, all they need to do is to tap the "GET" button and allow the device to verify with FaceID and the app will be installed.
                </p>
                <div class="d-flex justify-content-center">
                <img src="{$imagedir}{$appstore}" style="width: 117px; height: 253px" alt="iOS Logo">
                </div>
                
                <h2 class="text-center mt-4 underline"> iOS Feature: Lock Screen </h2>
                <p>
                    As iOS allows for a device to be locked behind a password and/or biometric authentication, a user will first met with a lock screen whenever they use their phone.
                    Between iOS 15 and 16 the device locking processs has been kept the same however there are a few ways where the customization of the lock screen has changed.
                </p>
                <p>  
                    iOS 15 is very limited compared to iOS 16, only being able to change the background of the lock screen.
                    iOS 16 allows for a few more bits of customization:
                </p>
                <li>The background image can have a "Depth Effect" applied allowing part of the image to show over the clock. Like the first image below.</li>
                <li>The clock font and colour can be changed</li>
                <li>Widgets such as battery meters for the phone and connected devices and new email notifications can be added to the lock screen. Like the second image below.</li>
                <div class="d-flex justify-content-center">
                <img src="{$imagedir}{$deptheffect}" class="mx-3" style="width: 117px; height: 253px" alt="iOS Logo">
                <img src="{$imagedir}{$widget}" class="mx-3" style="width: 117px; height: 253px" alt="iOS Logo">
                </div>
                
                <h2 class="text-center mt-4 underline"> IoS Feature - Notifications </h2>
                <p>
                    Notifications are a simple way that iOS can let the user know of various things going on in their device. Such as incoming texts, calls and messages.
                    When recieved, they will appear as a card at the top of the screen for a few moments before sliding away.
                    At that point they can be recalled by swiping down from the top of the screen to get to the notifications center.
                </p>
                <p>
                    Notifications have been redesigned between iOS 15 and iOS 16.
                </p>
                <p>    
                    iOS 15 notifications are static, only displaying the information they had at the time they were pushed to your devices.
                    iOS 16 notifications have been made dynamic. Allowing them to be updated in real time, for example, showing you real time location data while waiting for an uber to pick you up.
                </p>
                
                <h2 class="text-center mt-4 underline"> iOS Feature - iMessage </h2>
                <p>
                    iMessage is one of the built in app's under iOS and is the OS's primary way of sending SMS messages to other devices.
                </p>
                <p>
                    On top of usual messaging features such as SMS and MMS support iMessage has a few extra features that are specific to Apple devices.
                    Messages can be sent with special effects, visible only to other iOS users. Added to a message by holding onto the send button to open the effect interface.
                    Reactions can be added to text messages by pressing and holding on a message to open the reactions interface.
                    And more features.
                </p>
                <p>
                    iOS 16 expanded on the features offered by iOS 15 by allowing users to edit or unsend images that have been sent (Within a 15 minute time limit) as well as being able to mark a previously read message as unread.
                </p>
                <div class="d-flex mb-6 justify-content-center">
                <img src="{$imagedir}{$imsg}" style="width: 117px; height: 200px" alt="iOS Logo">
                </div>
            </article>
        </div>
    
    </div>
    
    PAGE;
    return $content;
}


{
    $pagecontent = createPage($showcase, $articles);
    $tabtitle = "Device Page";

    // build html
    $page = new MasterPage($tabtitle);
    $page->setDynamicContent2($pagecontent);
    $page->renderPage();
}

?>