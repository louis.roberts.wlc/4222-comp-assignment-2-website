function validateSignUpForm() 
{
	
    //Read in the form data
    let firstname = document.forms["signup"]["firstname"].value;
    let lastname = document.forms["signup"]["lastname"].value;
    let email = document.forms["signup"]["email"].value;
    let password = document.forms["signup"]["password"].value;
    let confirmpass = document.forms["signup"]["confirmpassword"].value;

    /*
        Set empty strings for each error message.
        These will be populated with an appropriate error message and concatenated into a final error
        if conditions are right to form the final alert.
    */
    var firstnameerr = "";
    var lastnameerr = "";
    var passwordrequirederr = "";
    var passnotmatcherr = "";
    var emailerr = "";
    var errorinit = "The following errors have occured:\n";
    //Assume form to be valid until conditions are met
    var formvalid = true;
    

    //Test each field to see if it exists.
    if (firstname === "")
    {
        firstnameerr = "First Name Required\n"
        formvalid = false;
    }
    if (lastname === "")
    {
        lastnameerr = "Last Name Required\n"
        formvalid = false;
    }
    if (email === "")
    {
        emailerr = "Email Address Required"
        formvalid = false;
    }
    if (password === "")
    {
        passwordrequirederr = "Password Required\n"
        formvalid = false;
    }
    if (password !== confirmpass)
    {
        passnotmatcherr = "Password fields are not identical\n"
        formvalid = false;
    }
    //if any required field isn't filled out. Generate the error message and display as an alert.
    if (formvalid === false)
    {
        errormsg = errorinit.concat(firstnameerr,lastnameerr,passwordrequirederr,passnotmatcherr,emailerr);
        alert(errormsg);
    }
    //Otherwise the form gets submitted for server serverside validation.
} 


function testButton()
{
	alert("This is a test")
}