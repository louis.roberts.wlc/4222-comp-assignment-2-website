<?php
//----INCLUDE APIS------------------------------------
include("api/api.inc.php");

session_start();

$action = $_REQUEST["action"] ?? "";
$token = $_SESSION["usrLoggedIn"] ?? "";

if($action == "exit" && !empty($token))
{
    appSessionEmptyTokens();
    appSessionDestroy();
    appGoToHome();
}
else
{
    appGoToError();
}

?>