<?php

session_start();

include("api/api.inc.php");

$formdata = formProcess($_REQUEST) ?? array();

if($_SERVER["REQUEST_METHOD"] == "POST")
{   
    
    $score = appFormProcessData($_REQUEST["score"]);
    $newusrreview = new bllUsrReview();
    $newusrreview->firstname = appFormProcessData($_SESSION["usrFirstName"] ?? "");
    $newusrreview->lastname = appFormProcessData($_SESSION["usrLastName"] ?? "");
    $newusrreview->deviceid = (int)$_REQUEST["id"];
    $newusrreview->reviewtext = appFormProcessData($_REQUEST["reviewcontent"] ?? "");
    $newusrreview->score = (int)$score;
    $isformvalid = true;
    //Check to see if any form data is missing.
    if($newusrreview->firstname == "")
    {
        $isformvalid = false;
    }
    if($newusrreview->lastname == "")
    {
        $isformvalid = false;
    }
    if($newusrreview->deviceid == "")
    {
        $isformvalid = false;
    }
    if($newusrreview->reviewtext == "")
    {
        $isformvalid = false;
    }
    //If any data is missing send the user to the error page
    if($isformvalid == false)
    {
        appGoToError();
    }
    else //create a new json object for the review
    {
        $id = jsonNextUsrReviewID();
        $newusrreview->id = $id;
        $saveusrreview = json_encode($newusrreview).PHP_EOL;
        $file = file_get_contents("data/json/usrreviews.json");
        $file .= $saveusrreview;
        
        //Write the new json object to the appropriate file.
        file_put_contents("data/json/usrreviews.json", $file);
    }
}

function formProcess(array $formdata): array
{
    
    foreach ($formdata as $field => $value)
    {
        $formdata[$field] = appFormProcessData($value);
    }
    $isformvalid = true;
    if ($isformvalid && empty($formdata["reviewcontent"]))
    {
        $isformvalid = false;
    }
    if ($isformvalid && empty($formdata["score"]))
    {
    }
    if($isformvalid)
    {
        $formdata["valid"] = true;
    }
    return $formdata;
}

function createPage($pdevices, $previews, $pusrreviews)
{
    $devicespecs = "";
    $extreviews = "";
    foreach($pdevices as $item)
    {
        $devicespecs .= renderDeviceSpecs($item);
    }
    foreach($previews as $item)
    {
        $extreviews .= renderExtDeviceReviews($item);
    }
    foreach($pusrreviews as $item)
    {
        $usrreview .= renderUsrReviewCard($item);
    }
    if (isset($_SESSION["usrLoggedIn"]))
    {
        $formmethod = "POST";
        $formaction = htmlspecialchars($_SERVER['PHP_SELF']);
        $leavereview = <<<FORM
		<div class="accordion" id="usrSendReview">
		<div class="accordion-item">
			<h2 class="accordion-header" id="usrSendReviewHeader">
				<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#usrSendReviewCollapse" aria-expanded="false" aria-controls="usrSendReviewCollapse">
 					Send a review
				</button>
			</h2>
			<div id="usrSendReviewCollapse" class="accordion-collapse collapse" aria-labelledby="usrSendReviewHeader" data-bs-parent="usrSendReview">
                <div class="my-3 text-center">
                    <p> Sending a review as: {$_SESSION["usrFirstName"]} {$_SESSION["usrLastName"]} </p>
                </div>
                <div id="reviewform" class="container bg-light shadow-lg mb-3">
                    <form id="review" name="review" action="{$formaction}?id={$_GET["id"]}" method="{$formmethod}" role="form" class="navbar-form navbar-right" onsubmit="">
                    <div class="mb-4 form-group pt-3">
                        <label for="review" class="form-label">Please write your review here:</label>
                        <textarea name="reviewcontent" class="form-control" id="reviewcontent" value="{$formdata["reviewcontent"]}" aria-describedby="reviewcontentHelp"></textarea>
                    </div>
                    <div class="mb-4 form-group pt-3">
                        <label for="review" class="form-label">What score would you give the device? (1-10):</label>
                        <input type="number" name="score" class="form-control" id="score" value="{$formdata["score"]}" min="1" max="10" aria-describedby="reviewcontentHelp"></textarea>
                    </div>
                        <button type="submit" class="btn btn-primary mb-4">Submit Review</button>
                    </form>
                </div>
            </div>
        </div>
        </div>
FORM;
    }
    else
    {
        $leavereview = "";
    }
    
    $content = <<<PAGE
    {$devicespecs}
    {$extreviews}
		<div class="accordion" id="usrReview">
		<div class="accordion-item">
			<h2 class="accordion-header" id="usrReviewHeader">
				<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#usrReviewCollapse" aria-expanded="false" aria-controls="usrReviewCollapse">
 					Show User Reviews
				</button>
			</h2>
			<div id="usrReviewCollapse" class="accordion-collapse collapse" aria-labelledby="usrReviewHeader" data-bs-parent="usrReview">
                    {$usrreview}
			</div>
        </div>
        </div>
        {$leavereview}
PAGE;
        return $content;
}

#business logic
$reviews = [];
$devices = [];
$deviceid = $_REQUEST["id"] ?? -1;

//Check if we have devices

if (is_numeric($deviceid) && $deviceid >= 0)
{
    $device = jsonLoadSingleDevice($deviceid);
    $devices[] = $device;
    $reviews[] = jsonLoadSingleExtReview($deviceid);
    
    //setup temp vars for user reviews
    $tempusrreviews = [];
    $tempusrreviews = jsonLoadAllUsrReview();
    //final user reviews will be dropped here
    $usrreviews = [];
    //Check held user reviews to see if they're for the current device page.
    foreach($tempusrreviews as $tempusrreview)
    {
        if($tempusrreview->deviceid == $deviceid)
        {
            //if they match, add it to the final array.
            array_push($usrreviews,$tempusrreview);
        }
    }
}   

//Page Decision - Device found?

if(count($devices)===0)
{
    appGoToError();
}
else
{
    $pagecontent = createPage($devices,$reviews,$usrreviews);
    $tabtitle = "Device Page";
    
    //build html
    
    $page = new MasterPage($tabtitle);
    $page->setDynamicContent2($pagecontent);
    $page->renderPage();
}

?>