<?php
require_once("bll-OO.inc.php");
//require_once("presentation-OO.inc.php");

// Render Business logic stuff

//Rendering article previews

function renderShowcasePreview()
{
    $imgref = "img/OS/IOS_logo.svg";
    $header = <<<HEADER
		<div class="col-md">
			<h2>Featured Showcase</h2>
			<div class="row mx-1 pb-2 g-10 border rounded overflow-hidden flex-md-row mb-4 shadow-lg h-md-250position-relative">
				<div class="col-auto">
					<img src="{$imgref}" class="image-flex mt-2" style="width: 200px; height: 200px" alt="iOS Logo">
				</div>
                <div class="col p-4 d-flex flex-column position-static">
                	<h3 class="mb-2">OS Showcase  <span class="badge bg-success align-middle">New!</span></h3>
                	<p class="card-text mb-auto">iOS 16 is the latest version of Apple's mobile Operating System for the iPhone. </p>
                	<a href="ios-showcase.php" class="text-end">View Article</a>
                </div>
            </div>
        </div>
    <h2>Featured Reviews</h2>
    <div class="row mb-2">

HEADER;
    return $header;
}

function renderArticlePreview(bllArticlePreview $item)
{                          //$imgpath example = preview01.png
    $imgref = "img/device/{$item->imgpath}.png";
    $itemsrc = file_exists($imgref) ? $imgref : "img/err.png";
    $articlepreview = <<<PREVIEW
		<div class="col-md-6">
			<div class="row mx-1 pb-2 border roundedds mb-4 shadow-lg">
				<div class="col-auto d-none d-lg-block">
					<img src="{$itemsrc}" class="image-flex mt-2" style="width: 146px; height: 200px" alt="iPhone 14">
            	</div>
            <div class="col p-4 d-flex flex-column">
            	<h3 class="mb-1">{$item->text} Review {$item->new}</h3>
            	<p class="card-text mb-auto">{$item->blurb}</p>
            	<a href="device.php?id={$item->id}" class="text-end">View Device</a>
            </div>
            </div>
        </div>

PREVIEW;
    return $articlepreview;
}

function renderDeviceTable(bllDevice $item)
{
    $tablecontent = <<<TABLE
        <tr>
            <td><a href="device.php?id={$item->id}" class="text-end">{$item->devicename}</a></td>
            <td>{$item->type}</td>
            <td>{$item->manufacturername}</td>
            <td>{$item->batterysize}</td>
            <td>{$item->osversion}</td>
            <td>{$item->processorname}</td>
            <td>{$item->corecount}</td>
            <td>{$item->coredetails}</td>
            <td>{$item->releasedate}</td>
            <td>{$item->score}/10</td>
        </tr>
TABLE;
    
    return $tablecontent;
    
}

function renderUsrReviewCard(bllUsrReview $item)
{
    $firstname = $item->firstname;
    $lastname = $item->lastname;
    $score = $item->score;
    $reviewtext = $item->reviewtext;
    
    $content = <<<USR
    <div class="card mx-auto my-3 shadow" style="width: 1200px;">
        <div class="card-body my-3">
            <h5 class="card-title">{$firstname} {$lastname}</h5>
            <p class="card-text">User Score: {$score}</p>
            <p class="card-text">{$reviewtext}</p>
        </div>
    </div>
USR;
    
    return $content;
    
}

function renderDeviceSpecs(bllDevice $item)
{
    $imgref = "img/device/{$item->image}.png";
    $img = $imgref;
    
    $specs = <<<SPECS
    <!--renderDeviceSpecs - Presentation-fn.inc.php-->
	<div class="row container-fluid">
		<div class="col-md text-center mb-2">
			<h2>{$item->type} Overview - {$item->devicename}</h2>
		</div>
	</div>
	<div class="row container-fluid mb-3">
		<div class="col-fluid justify-content-md-center text-center">
			<img src="{$img}" class="image-fluid shadow-sm" style="width: 200px; height: 200px" alt="iOS Logo">		
		</div>
	</div>
	<div class="row container-fluid">
		<div class="accordion" id="deviceSpecs">
			<div class="accordion-item">
				<h2 class="accordion-header" id="specsHeader">
					<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#specsCollapse" aria-expanded="false" aria-controls="specsCollapse">
	 					Show Device Specifications
					</button>
				</h2>
				<div id="specsCollapse" class="accordion-collapse collapse" aria-labelledby="specsHeader" data-bs-parent="#deviceSpecs">
					<div class="accordion-body">
						<table class="table">
						  <thead>
						    <tr>
						      <th scope="col">Manufacturer</th>
						      <th scope="col">Battery Capacity</th>
						      <th scope="col">OS</th>
						      <th scope="col">Processor</th>
						      <th scope="col">Core Count</th>
						      <th scope="col">Core Details</th>
						      <th scope="col">Release Date</th>
                              <th scope="col">Overall TDR Score</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr>
						      <td>{$item->manufacturername}</td>
						      <td>{$item->batterysize}</td>
						      <td>{$item->osversion}</td>
						      <td>{$item->processorname}</td>
						      <td>{$item->corecount}</td>
						      <td>{$item->coredetails}</td>
						      <td>{$item->releasedate}</td>
                              <td>{$item->score}/10</td>
						    </tr>
						  </tbody>
						</table>
	  				</div>
				</div>
			</div>
		</div>
		<div class="accordion" id="deviceArticle">
			<div class="accordion-item">
				<h2 class="accordion-header" id="ArticleHeader">
					<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#ArticleCollapse" aria-expanded="false" aria-controls="ArticleCollapse">
	 					Show our review of the {$item->devicename}
					</button>
				</h2>
				<div id="ArticleCollapse" class="accordion-collapse collapse" aria-labelledby="ArticleHeader" data-bs-parent="#deviceSpecs">
					<div class="accordion-body">
                        {$item->articletxt}
                        <br>
                        <div class="text-center"><img src="img/score/{$item->score}.png" class="center-block img-responsive" style="width: 200px; height: 200px" alt="{$item->score}/10"></div>
	  				</div>
				</div>
            </div>
        </div>
SPECS;
    return $specs;
}

function renderExtDeviceReviews(bllExtReview $item)
{
    $reviews = <<<REVIEWS
		<div class="accordion" id="deviceReviews">
			<div class="accordion-item">
				<h2 class="accordion-header" id="extReviewHeader">
					<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#extReviewCollapse" aria-expanded="false" aria-controls="extReviewCollapse">
	 					Show Device reviews
					</button>
				</h2>
				<div id="extReviewCollapse" class="accordion-collapse collapse" aria-labelledby="extReviewHeader" data-bs-parent="#deviceSpecs">
					<div class="accordion-body">
						<table class="table">
						  <thead>
						    <tr>
						      <th scope="col">Review Source</th>
						      <th scope="col">Link</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr>
						      <td>{$item->source}</td>
						      <td><a href="{$item->sitelink}">{$item->sitelink}</a></td>
						    </tr>
						    <tr>
						      <td>{$item->source2}</td>
						      <td><a href="{$item->sitelink2}">{$item->sitelink2}</a></td>
						    </tr>
						    <tr>
						      <td>{$item->source3}</td>
						      <td><a href="{$item->sitelink3}">{$item->sitelink3}</a></td>
						    </tr>
						  </tbody>
						</table>
	  				</div>
				</div>
            </div>
        </div>
REVIEWS;
    
    return $reviews;

}
?>