<?php

//represents standard HTML page
class HTMLPage
{
    private $_css_dir  = "";
    private $_dat_dir  = "";
    private $_img_dir  = "";
    private $_js_dir   = "";
    
    
    private $_css_arr   = [];
    private $_js_arr    = [];
    private $_meta_arr  = [];
    
    private $_tab_title      = "";
    private $_head_otherhtml = ""; //Store HTML to go in <Head>
    
    private $_body_content   = ""; //Store HTML to go in <Body>
    
    function __construct($tab_title)
    {
        $this->_tab_title = $tab_title;
    }
    
    
    
    
    //SETTERS
    
    //Add Files
    public function addCSS($cssfile)
    {
        $this->_css_arr[] = $cssfile;
    }
    
    public function addScript($script_file)
    {
        $this->_js_arr[] = $script_file;
    }
    
    //Set the tab label
    public function setTabTitle($tabTitle)
    {
        $this->_tab_title;
    }
    
    //Add HTML tags
    public function addMeta($metakey,$metavalue)
    {
        $this->_meta_arr[$metakey] = $metavalue;
    }
    
    public function setCustomHead($otherHeadHTML)
    {
        $this->_head_otherhtml;
    }
    
    //Set directories
    public function setCSSDir($cssDir)
    {
        $this->_css_dir = $cssDir;
    }
    
    public function setDataDir($datDir)
    {
        $this->_dat_dir = $datDir;
    }
    
    public function setImgDir($imgDIR)
    {
        $this->_img_dir = $imgDIR;
    }
    
    public function setJSDir($jsDir)
    {
        $this->_js_dir = $jsDir;
    }
    
    //Set content for <body>
    public function setBodyContent($bodyCont)
    {
        $this->_body_content = $bodyCont;
    }
    
    //!!Getters!!
    public function getArrScriptFiles($path = false)
    {
        if($path)
        {
            return $this->toURLs($this->_css_arr,$this->_dir_css);
        }
        //else
        return $this->_css_arr;
    }
    
    public function getTabTitle()
    {
        return $this->_tab_title;
    }
    
    public function getCSSDir()
    {
        return $this->_css_dir;
    }
    
    public function getDataDir()
    {
        return $this->_dat_dir;
    }
    
    public function getImgDir()
    {
        return $this->_img_dir;
    }
    
    public function getJSDir()
    {
        return $this->_js_dir;
    }
    
    
    
    public function getBodyContent()
    {
        return $this->_body_content;
    }
    
    
    


    //private funcs
    
    private function createMeta()
    {
        $html = "";
        foreach($this->_meta_arr as $name => $value)
        {
            $meta = <<<META
<meta name="{$name}" value="{$value}">
META;
            $html.=$meta;
        }
        return $html;
        
    }
    
    private function createCSS()
    {
        $html = "";
        $csspath = $this->toURLs($this->_css_arr,$this->_css_dir);
        foreach($csspath as $cssfile)
        {
            $cssmarkup = <<<CSS
<link href="{$cssfile}" rel="stylesheet">
CSS;
            $html.=$cssmarkup;
        }
        return $html;
    }
    
    private function createHead()
    {
        $head = <<<HEAD
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--Meta Tags-->
    {$this->createMeta()}
    <!--Include External CSS-->
    {$this->createCSS()}
    <title>{$this->_tab_title}</title>
</head>
HEAD;
    return $head;
    }
    
    //Generate the body and associated content
    
    private function createBody()
    {
        $this->createJS();
        $html = <<<BODY
<body class="d-flex flex-column min-vh-100">
    <!--PHP Generated content-->

    {$this->_body_content}
    

    <!-- Include JS scripts -->
    {$this->createJS()}

</body>

BODY;
    return $html;
    }
    
    private function createJS()
    {
        $html = "";
        $jspath = $this->toURLs($this->_js_arr, $this->_js_dir);
        foreach($jspath as $jsfile)
        {
            $markup = <<<JS
<script src="{$jsfile}"></script>
JS;
            $html .= $markup;
        }
        return $html;
    }

    
    
    
    
    //Public funcs

    //Creates the "Shell" of a page.
    public function createPage()
    {
        $markup = <<<HTML
<!DOCTYPE html>
<html lang="en">
<!--Head Content-->
{$this->createHead()}
<!--Body content-->
{$this->createBody()}
</html>
HTML;
        return $markup;
    }
    
    //Prints out final page content.
    public function renderPage()
    {
        echo $this->createPage();
    }
    
    //Construct dir paths for CSS, JS, Images and Misc DATA
    public function setDirs($CSS, $IMG, $JS, $DAT)
    {
        $this->setCSSDir($CSS);
        $this->setJSDir($JS);
        $this->setImgDir($IMG);
        $this->setDataDir($DAT);
    }

    //Misc Funcs
    
    //Generate set of URLs for includes (CSS, JS, Images, etc)
    function toURLs(array &$array,$path)
    {
        $patharray = [];
        foreach($array as $file)
        {
            $patharray[] = "{$path}/{$file}";
        }
        return $patharray;
    }
}

?>