<?php

class bllAccount
{
    //Fields
    public $id = null;
    public $favedeviceid;
    public $firstname;
    public $lastname;
    public $password;
    public $emailaddress;
    
    public function fromArray(stdClass $passoc)
    {
        foreach($passoc as $key => $value)
        {
            $this->{$key} = $value;
        }
    }
}



class bllDevice
{
    //Fields
    
    public $id = null;
    public $devicename;
    public $manufacturername;
    public $batterysize;
    public $releasedate;
    public $osversion;
    public $processorname;
    public $corecount;
    public $coredetails;
    public $score;
    public $image;
    public $type;
    public $articletxt;
    
    public function fromArray(stdClass $passoc)
    {
        foreach($passoc as $key => $value)
        {
            $this->{$key} = $value;
        }
    }
    
    
}


class bllExtReview
{
    public $id = null;
    public $deviceid;
    public $source;
    public $sitelink;
    public $type;
    
    public function fromArray(stdClass $passoc)
    {
        foreach($passoc as $key => $value)
        {
            $this->{$key} = $value;
        }
    }
}



class bllretailer
{
    public $id = null;
    public $deviceid;
    public $retailer;
    public $price;
    public $sitelink;
    
    public function fromArray(stdClass $passoc)
    {
        foreach($passoc as $key => $value)
        {
            $this->{$key} = $value;
        }
    }
}
 


class bllUsrReview
{
    public $id = null;
    public $deviceid;
    public $firstname;
    public $lastname;
    public $score;
    public $reviewtext;
    
    public function fromArray(stdClass $passoc)
    {
        foreach($passoc as $key => $value)
        {
            $this->{$key} = $value;
        }
    }
}



class bllArticlePreview
{
    public $id = 0;
    public $text;
    public $imgpath;
    public $itempath;
    public $new;
    public $blurb;
    public $pageref;
    
    public function fromArray(stdClass $passoc)
    {
        foreach($passoc as $key => $value)
        {
            $this->{$key} = $value;
        }
    }
}




?>