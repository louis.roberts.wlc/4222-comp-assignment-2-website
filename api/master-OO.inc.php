<?php

//This reprents master layout

//Include Page class
require_once ("page-OO.inc.php");

class MasterPage
{
    //Members
    private $_mainHTML;
    private $_dynamicContent1;
    private $_dynamicContent2;
    private $_dynamicContent3;
    private $_dynamicContent4;
    
    
    

    //Constructor
    function __construct($tab_title)
    {
        $this->_mainHTML = new HTMLPage($tab_title);
        $this->setDefaults();
        $this->setDynDefaults();
    }
    
    
    
    
    //Getter/Setter
    public function getDynamicContent1()
    {
        return $this->_dynamicContent1;
    }
    
    public function getDynamicContent2()
    {
        return $this->_dynamicContent2;
    }

    public function getDynamicContent3()
    {
        return $this->_dynamicContent3;
    }
    
    public function getDynamicContent4()
    {
        return $this->_dynamicContent3;
    }
    
    public function getDynamicContent5()
    {
        return $this->_dynamicContent3;
    }
    
    public function setDynamicContent1($html)
    {
        $this->_dynamicContent1 = $html;
    }
    
    public function setDynamicContent2($html)
    {
        $this->_dynamicContent2 = $html;
    }
    
    public function setDynamicContent3($html)
    {
        $this->_dynamicContent2 = $html;
    }
    
    public function setDynamicContent4($html)
    {
        $this->_dynamicContent2 = $html;
    }
    
    //Private Funcs
    //Set default vals for a page
    private function setDefaults()
    {
        $this->_mainHTML->setDirs("css", "img", "js", "data");
        //add CSS files - Order important
        $this->addCSS("bootstrap.css");
        $this->addCSS("site.css");
        //add JS files - Order important
        $this->addJS("custom.js");
        $this->addJS("jquery-3.6.4.min.js");
        //popper to get some bootstrap stuff working
        $this->addJS("popper-v2.11.6.js");
        $this->addJS("bootstrap.js");
    }
    
    private function setDynDefaults()
    {
        $currentyr = date("Y");
        $this->_dynamicContent1 = <<<TITLE

TITLE;
        $this->_dynamicContent2 = "";
        
        $this->_dynamicContent3 = <<<FOOTER
           <footer class="footer mt-auto">
                <div class="bottom">
                    <div class="container">
                        <div class="row">
                            <div class="flex-column col text-end">
                            <p>&copy {$currentyr} The Device Review. All rights reserved.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        FOOTER;
    }
    
    private function setMaster()
    {
        $login = "app_entry.php";
        
        $logout = "app_exit.php";
        
        $entryHTML = <<<LOGIN
        <!--master-OO.inc.php-->
                <a class="btn btn-primary navbar-right" href="signin.php">Sign-In</a>
                <a class="btn btn-secondary navbar-right" href="signup.php">Sign-Up</a>
        <!--/master-OO.inc.php-->
LOGIN;
        
        $exitHTML = <<<EXIT
        <!--master-OO.inc.php-->
        <a class="btn btn-info navbar-right" href="{$logout}?action=exit">Sign-Out</a>
        <!--/master-OO.inc.php-->
EXIT;
        
        $auth = isset($_SESSION["myuser"]) ? $exitHTML : $entryHTML;
        
        if((isset($_SESSION["usrLoggedIn"])) && ($_SESSION["usrLoggedIn"] == true))
        {
            $auth = $exitHTML;
        }
        else
        {
            $auth = $entryHTML;
        }
        
        if((isset($_SESSION["usrLoggedIn"])) && ($_SESSION["usrLoggedIn"] == true))
        {
            $name = "Logged in as: ";
            $name .= $_SESSION["usrEmail"];
            $name .= " - ";
            $name .= $_SESSION["usrFirstName"];
            $name .= " ";
            $name .= $_SESSION["usrLastName"];
        }
        else 
        {
            $name = "";
        }
        
        $masterpage = <<<MASTER
<!-- Navigation -->
<!-- Navbar from https://bootswatch.com/default/ -->
<nav class="navbar navbar-expand navbar-dark bg-dark">
	<div id="nav" class=container>
	  <div class="container-fluid">
	    <div class="collapse navbar-collapse" id="navbarColor02">
	      <ul class="navbar-nav me-auto">
	        <li class="nav-item">
	          <a class="nav-link active" href="index.php">Home</a>
	        </li>
	          <a class="nav-link" href="devicelist.php">Device List</a>
	          <a class="nav-link" href="ios-showcase.php">iOS showcase</a>
              <a class="nav-link" href="userprofile.php">{$name}</p>
	      </ul>
			{$auth}
	    </div>
	  </div>
	</div>
</nav>

<!-- Main Page Content -->
<div id="main" class="container">
    <div class="header">
        <h1 class="display-1 text-center">The Device Review</h1>
        <p class="lead text-center underline"> Bringing you the lastest tech reviews.</p>
    </div>
    {$this->_dynamicContent2}
    {$this->_dynamicContent4}
</div>
</div>


<!-- Footer -->
{$this->_dynamicContent3}

MASTER;
	
	   $this->_mainHTML->SetBodyContent($masterpage);
    }
    
    
    
    
    
    //Public funcs
    public function createPage()
    {
        //Create page and return
        $this->SetMaster();
        return $this->_mainHTML->createPage();
    }
    
    public function renderPage()
    {
        //Create page and echo
        $this->SetMaster();
        $this->_mainHTML->renderPage();
    }
    
    public function addCSS($cssfile)
    {
        $this->_mainHTML->addCSS($cssfile);
    }
    
    public function addJS($jsfile)
    {
        $this->_mainHTML->addScript($jsfile);
    }
    
    #Placeholder
    public function addnothing()
    {
        
    }
    
}


?>