<?php

require_once("bll-OO.inc.php");



#clean-up data from a form, regardless of value.

function appFormProcessDataWithNulls($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}


# Clean-up data from form.
# Convert null entries to blanks and skip

function appFormProcessData($data)
{
    $clean = $data ?? "";
    if (! empty($clean))
    {
        $clean = trim($clean);
        $clean = stripslashes($clean);
        $clean = htmlspecialchars($clean);
    }
    return $clean;
}


#Add Form Validity key - to be used with Check Function.

function appFormSetValid(array &$data)
{
    $data["valid"] = true;
}


# Check for Form Validity Key

function appFormCheckValid(array $data)
{
    return array_key_exists("valid",$data);
}


# Treat Null values and missing keys as empty.

function appFormNullToEmpty(array &$data, $key)
{
    $data[$key] = $data[$key] ?? "";
}

//Check to see if the server has been sent a POST request.
function appFormMethodIsPost()
{
    return strtolower($_SERVER['REQUEST_METHOD']) == 'post';
}


# Get form action to Self-Submit.

function appFormActionSelf()
{
    return htmlspecialchars($_SERVER['PHP_SELF']);
}


# Get the form method.
function appFormMethod($defaultmethod = true)
{
    return $defaultmethod ? "POST" : "GET";
}


# paginate an array using the slice technique

function appPaginateArray(array $array,$pagenum,$numofitems)
{
    $pagenum = $pagenum < 1 ? 1 : $pagenum;
    $start = ($pagenum - 1) * $numofitems;
    return array_slice($array, $start, $numofitems);
}


# Paginate an array using the chunk technique.

function appPaginateArrayAlt(array $array, $pagenum,$numofitems)
{
    $arrayofpages = array_chunk($array, $numofitems);
    return $pagenum > sizeof($arrayofpages) ? [] : $arrayofpages[$pagenum - 1];
}



# Redirect To Home Page
function appGoToHome()
{
    header("Location: index.php");
}


# Redirect To Error Page
function appGoToError()
{
    header("Location: app_error.php");
}

function appGoToSignupSuccess()
{
    header("Location: signupsuccess.php");
}

function appGoToSigninSuccess()
{
    header("Location: signinsuccess.php");
}


# Initialise the common session keys for this application.
function appSessionInitData(bool $start = true)
{
    if($start)
    {
        session_start();
    }
    $tsession = ["myname","myuser","fav-device"];
    foreach($tsession as $tsessionkey)
    {
        $_SESSION[$tsessionkey] = "";
    }
}


# Check for existence of the login loken
function appSessionLoginExists()
{
    $tuser = $_SESSION["myuser"] ?? "";
    if(!empty($tuser))
        return true;
        return false;
}


# Create the Login Tokens

function appSessionSetLoginTokens($usrname)
{
    if(!empty($usrname))
    {
        $_SESSION["myuser"] = processRequest($usrname);
        $_SESSION["entered"] = true;
    }
}


# Create the login tokens and redirect to the Home Page
function appSessionLoginAndReturn($usrname)
{
    setLoginTokens($usrname);
    goToHome();
}


# reset all tokens by unsetting them
# this preserves the actual session.

function appSessionUnsetTokens()
{
    foreach(array_keys($_SESSION) as $key)
    {
        unset($_SESSION[$key]);
    }
}

// Reset all tokens to empty and clear the login token.
function appSessionEmptyTokens()
{
    foreach(array_keys($_SESSION) as $key)
    {
        $_SESSION[$key] = "";
    }
    unset($_SESSION["entered"]);
}


# destroy the session and unset existing tokens.
function appSessionDestroy()
{
    session_unset();
    session_destroy();
}

?>