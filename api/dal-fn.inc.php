<?php
require_once("bll-OO.inc.php");
require_once("presentation-OO.inc.php");

//Functions to help with JSON stuff

//return one json instance matching a specified ID
function jsonSingleInstance($file,$id)
{
    $splfile = new SplFileObject($file);
    $splfile->seek($id-1);
    $data = json_decode($splfile->current());
    return $data;
}

//Return all JSON entries as an array
function jsonAllInstances($file)
{
    $entries = file($file);
    $jsonarray = [];
    foreach($entries as $entry)
    {
        $jsonarray[] = json_decode($entry);
    }
        return $jsonarray;
}

//Returns the next available ID in a json file. If ID's 1-6 are taken, this will return a 7.
function jsonNextID($file)
{
    $splfile = new SplFileObject($file);
    $splfile->seek(PHP_INT_MAX);
    return $splfile->key() + 1;
}

//returns the next available ID for the devices JSON file as per the above func
function jsonNextDeviceID()
{
    return jsonNextID("data/json/devices");
}

function jsonNextUserID()
{
    return jsonNextID("data/json/accdetails.json");
}

function jsonNextUsrReviewID()
{
    return jsonNextID("data/json/usrreviews.json");
}

// JSON - return single instance based on ID

function jsonLoadSingleAccount($id) : bllAccount
{
    $account = new bllAccount();
    $account->fromArray(jsonSingleInstance("data/json/accdetails.json", $id));
    return $account;
}

function jsonLoadSingleDevice($id) : bllDevice
{
    $device = new bllDevice();
    $device->fromArray(jsonSingleInstance("data/json/devices.json",$id));
    return $device;
}

function jsonLoadSingleExtReview($id) : bllExtReview
{
    $extreview = new bllExtReview();
    $extreview->fromArray(jsonSingleInstance("data/json/extreview.json", $id));
    return $extreview;
}

function jsonLoadSingleRetailer($id) : bllretailer
{
    $retailer = new bllretailer();
    $retailer->fromArray(jsonSingleInstance("data/json/retailer.json", $id));
    return $retailer;
}

function jsonLoadSingleUsrReview($id) : bllUsrReview
{
    $usrreview = new bllUsrReview();
    $usrreview->fromArray(jsonSingleInstance("data/json/usrreviews.json", $id));
}

//May not need this, but just in case
function jsonLoadSingleArticle($id) : bllArticlePreview
{
    $usrreview = new bllArticlePreview();
    $usrreview->fromArray(jsonSingleInstance("data/json/articlepreview.json", $id));
}

// JSON - return all JSON instances of a particular file in an array



function jsonLoadAllAccount() : array
{
    $array = jsonAllInstances("data/json/accdetails.json");
    return array_map(function($a){ $tc = new bllAccount(); $tc->fromArray($a); return $tc; },$array);
}

function jsonLoadAllDevices() : array
{
    $array = jsonAllInstances("data/json/devices.json");
    return array_map(function($a){ $tc = new bllDevice(); $tc->fromArray($a); return $tc; },$array);
}

function jsonLoadAllExtReview() : array
{
    $array = jsonAllInstances("data/json/extreview.json");
    return array_map(function($a){ $tc = new bllExtReview(); $tc->fromArray($a); return $tc; },$array);
}

function jsonLoadAllRetailer() : array
{
    $array = jsonAllInstances("data/json/retailer.json");
    return array_map(function($a){ $tc = new bllretailer(); $tc->fromArray($a); return $tc; },$array);
}

function jsonLoadAllUsrReview() : array
{
    $array = jsonAllInstances("data/json/usrreviews.json");
    return array_map(function($a){ $tc = new bllUsrReview(); $tc->fromArray($a); return $tc; },$array);
}

function jsonLoadAllArticlePreviews() : array
{
    $array = jsonAllInstances("data/json/articlepreview.json");
    return array_map(function($a){ $tc = new bllArticlePreview(); $tc->fromArray($a); return $tc; },$array);
}

// XML Handling - Similar to above

function xmlLoadAllInstances($xmlfile, $class, $arrayname)
{
    //Load XML data from a given file
    $data = simplexml_load_file($xmlfile, $class);
    $array = [];
    foreach($data->{$arrayname} as $element)
    {
        $array[] = $element;
    }
    return $array;
}

function xmlLoadOneInstance($xmlfile,$class)
{
    $xmldata = simplexml_load_file($xmlfile, $class);
    return $xmldata;
}


?>
