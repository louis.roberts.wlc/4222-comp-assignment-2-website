<?php

session_start();

include("api/api.inc.php");

#page generation
$formmethod = "POST";
$formaction = htmlspecialchars($_SERVER['PHP_SELF']);
$formdata = formProcess($_REQUEST) ?? array();


if(isset($formdata["valid"]))
{
    $pagecontent = generateResponse($formdata);
}
else
{
    $pagecontent = createPage($formmethod, $formaction, $formdata);
}

//check to see if form data has been sent.
if($_SERVER["REQUEST_METHOD"] == "POST")
{   
    
    //A blank/throwaway account object for username/password comparison
    $login = new bllAccount();
    $login->password = appFormProcessData($_REQUEST["password"] ?? "");
    $login->emailaddress = appFormProcessData($_REQUEST["email"] ?? "");
    $isformvalid = true;
    //Check to see if any form data is missing.
    if($login->password == "")
    {
        $isformvalid = false;
    }
    if($login->emailaddress == "")
    {
        $isformvalid = false;
    }
    if($isformvalid == false)
    {
        $_SESSION["errmsg"] = "Incorrect login details entered.";
        appGoToError();
    }
    else
    {
        $accounts = [];
        $accounts = jsonLoadAllAccount();
        $accountCount = count($accounts);
        // compare user login details with saved account details.
        foreach ($accounts as $account)
        {            
            // If account details don't match but we havent gone through the entire account list yet.
            if (!($login->emailaddress == $account->emailaddress) && !($login->password == $account->password) && !($accountCount == 0))
            {
                //Decrement the account counter and check the next account and move on.
                --$accountCount;
            }
            // If the user has tried logging in with valid credentials.
            if (($login->emailaddress == $account->emailaddress) && ($login->password == $account->password))
            {
                //set session variables for the now logged in user and redirect
                $_SESSION["usrID"] = $account->id;
                $_SESSION["usrEmail"] = $account->emailaddress;
                $_SESSION["usrFirstName"] = $account->firstname;
                $_SESSION["usrLastName"] = $account->lastname;
                $_SESSION["usrLoggedIn"] = true;
                appGoToSigninSuccess();
            }
            if($accountCount == 0) // If we have checked all the accounts and no match was found.
            {
                $_SESSION["errmsg"] = "Incorrect login details entered.";
                appGoToError();
            }
        }

    }
}



function createPage($formmethod, $formaction, array $formdata)
{
    appFormNullToEmpty($formdata, "email");
    appFormNullToEmpty($formdata, "firstname");
    appFormNullToEmpty($formdata, "lastname");
    appFormNullToEmpty($formdata, "password");
    appFormNullToEmpty($formdata, "confirmpassword");
    
    $content = <<<PAGE
    <div class="row container-fluid">
        <div class="col-md text-center mb-2">
        <h2>Sign In</h2>
        </div>
    </div>
    </div>
    <div id="signinform" class="container bg-light shadow-lg mb-3">
        <form id="signin" name="signin" action="{$formaction}" method="{$formmethod}" role="form" class="navbar-form navbar-right" onsubmit="validateSignInForm()">
        <div class="mb-4 form-group pt-3">
            <label for="email" class="form-label">Email address:</label>
            <input type="email" name="email" class="form-control" id="email" value="{$formdata["email"]}" aria-describedby="emailHelp">
            {$formdata["erremail"]}
        </div>
        <div class="mb-4 form-group">
            <label for="password" class="form-label">Password:</label>
            <input type="password" name="password" class="form-control" value="{$formdata["password"]}" id="password">
            {$formdata["errpassword"]}
        </div>
        <div class="mb-4 form-check form-group">
            <input type="checkbox" name="rememberme" class="form-check-input" id="rememberme">
            <label class="form-check-label" for="rememberme">Remember Me</label>
        </div>
            <button type="submit" class="btn btn-primary mb-4">Sign Up</button>
        </form>
    </div>
PAGE;
            return $content;
}

function generateResponse(array $formdata)
{
    $response = <<<RESPONSE
<section class="panel panel-primary" id="response">
    <div class="container bg-light shadow-lg">
    <h1> Success! Thank you {$formdata["firstname"]} {$formdata["lastname"]}</h1>
    <p class="lead"> Account has been created. Thank you for signing up to The Device Review </p>
    </div>
</section>
RESPONSE;
    
    
    
    return $response;
}


function formProcess(array $formdata): array
{
    
    foreach ($formdata as $field => $value)
    {
        $formdata[$field] = appFormProcessData($value);
    }
    $isformvalid = true;
    if ($isformvalid && empty($formdata["password"]))
    {
        $isformvalid = false;
        $formdata["errpassword"] = "<p id=\"errpassword\" class=\"help-block\"> Password Required </p>";
    }
    if ($isformvalid && empty($formdata["email"]))
    {
        $isformvalid = false;
        $formdata["erremail"] = "<p id=\"erremail\" class=\"help-block\"> E-mail Required </p>";
    }
    if($isformvalid)
    {
        $formdata["valid"] = true;
    }
    return $formdata;
}

//check to see if the article previews loaded properly
$pagecontent = createPage($formmethod, $formaction, $formdata);
$tabtitle = "Sign In";

//build html
$page = new MasterPage($tabtitle);
$page->setDynamicContent2($pagecontent);
$page->renderPage();

?>