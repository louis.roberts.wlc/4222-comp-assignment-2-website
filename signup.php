<?php
include("api/api.inc.php");

session_start();

/*
For the purpose of the assignment, this signup page is NOT SECURE nor to a professional standard.
Account details are stored in plaintext.
*/

$formmethod = "POST";
$formaction = htmlspecialchars($_SERVER['PHP_SELF']);
$formdata = formProcess($_REQUEST) ?? array();

if (isset($formdata["valid"]))
{
    $pagecontent = generateResponse($formdata);
}
else
{
    $pagecontent = createPage($formmethod, $formaction, $formdata);
}

//check to see if form data has been sent.
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    $newuser = new bllAccount();
    $newuser->firstname = appFormProcessData($_REQUEST["firstname"] ?? "");
    $newuser->lastname = appFormProcessData($_REQUEST["lastname"] ?? "");
    $newuser->password = appFormProcessData($_REQUEST["password"] ?? "");
    $newuser->emailaddress = appFormProcessData($_REQUEST["email"] ?? "");
    $newuser->favedeviceid = 0; //Set this to 0 as the user will set this later in the profile pages.
    $isformvalid = true;
    //Check to see if any form data is missing.
    if($newuser->firstname == "")
    {
        $isformvalid = false;
    }
    if($newuser->lastname == "")
    {
        $isformvalid = false;
    }
    if($newuser->password == "")
    {
        $isformvalid = false;
    }
    if($newuser->emailaddress == "")
    {
        $isformvalid = false;
    }
    //If any data is missing send the user to the error page
    if($isformvalid == false)
    {
        appGoToError();
    }
    else //create a new json object for the user
    {
        $id = jsonNextUserID();
        $newuser->id = $id;
        $saveuser = json_encode($newuser).PHP_EOL;
        $file = file_get_contents("data/json/accdetails.json");
        $file .= $saveuser;

        //Write the new json object to the appropriate file.
        file_put_contents("data/json/accdetails.json", $file);
        /*
         * Finish by sending the user to a page that notifies them the account has been created
         * Passing a session token holding the users email address for the following page to display.
         */
        $_SESSION["newuseremail"] = $newuser->emailaddress;

        header("Location: signupsuccess.php");
    }
}


function createPage($formmethod, $formaction, array $formdata)
{
    appFormNullToEmpty($formdata, "email");
    appFormNullToEmpty($formdata, "firstname");
    appFormNullToEmpty($formdata, "lastname");
    appFormNullToEmpty($formdata, "password");
    appFormNullToEmpty($formdata, "confirmpassword");

    $content = <<<PAGE
    <div class="row container-fluid">
        <div class="col-md text-center mb-2">
        <h2>Sign Up</h2>			
        </div>
    </div>
    </div>
    <div id="signupform" class="container bg-light shadow-lg mb-3">
        <form id="signup" name="signup" action="{$formaction}" method="{$formmethod}" role="form" class="navbar-form navbar-right" onsubmit="validateSignUpForm()">
        <div class="mb-4 form-group pt-3">
            <label for="firstname" class="form-label">First Name:</label>
            <input type="text" name="firstname" class="form-control" value="{$formdata["firstname"]}" id="firstname">
            {$formdata["errfirstname"]}
        </div>
        <div class="mb-4 form-group">
            <label for="lastname" class="form-label">Last Name:</label>
            <input type="text" name="lastname" class="form-control" value="{$formdata["lastname"]}" id="lastname">
            {$formdata["errlastname"]}
        </div>
        <div class="mb-4 form-group">
            <label for="email" class="form-label">Email address:</label>
            <input type="email" name="email" class="form-control" id="email" value="{$formdata["email"]}" aria-describedby="emailHelp">
            {$formdata["erremail"]}
        </div>
        <div class="mb-4 form-group">
            <label for="password" class="form-label">Password:</label>
            <input type="password" name="password" class="form-control" value="{$formdata["password"]}" id="password">
            {$formdata["errpassword"]}
        </div>
        <div class="mb-4 form-group">
            <label for="confirmPassword" class="form-label">Confirm Password:</label>
            <input type="password" name="confirmpassword" class="form-control" value="{$formdata["confirmpassword"]}" id="confirmpassword">
            {$formdata["errconfpassword"]}
            {$formdata["erridenticalpass"]}
        </div>
        <div class="mb-4 form-check form-group">
            <input type="checkbox" name="rememberme" class="form-check-input" id="rememberme">
            <label class="form-check-label" for="rememberme">Remember Me</label>
        </div>
            <button type="submit" class="btn btn-primary mb-4">Sign Up</button>
        </form>
    </div>
PAGE;
    return $content;
}

function generateResponse(array $formdata)
{
    $response = <<<RESPONSE
<section class="panel panel-primary" id="response">
    <div class="container bg-light shadow-lg">
    <h1> Success! Thank you {$formdata["firstname"]} {$formdata["lastname"]}</h1>
    <p class="lead"> Account has been created. Thank you for signing up to The Device Review </p>
    </div>
</section>
RESPONSE;



    return $response;
}


function formProcess(array $formdata): array
{
    #test data for debugging
//     $formdata["firstname"] = "louis";
//     $formdata["lastname"] = "roberts";
//     $formdata["email"] = "test@test.test";
//     $formdata["password"] = "123";
//     $formdata["confirmpassword"] = "123";

    //Set these to blank otherwise the debugger complains.
    $formdata["errfirstname"] = "";
    $formdata["errlastname"] = "";
    $formdata["errpassword"] = "";
    $formdata["errconfpassword"] = "";
    $formdata["erridenticalpass"] = "";
    $formdata["erremail"] = "";

    foreach ($formdata as $field => $value)
    {
        $formdata[$field] = appFormProcessData($value);
    }
    $isformvalid = true;
    if ($isformvalid && empty($formdata["firstname"]))
    {
        $isformvalid = false;
        $formdata["errfirstname"] = "<p id=\"errfirstname\" class=\"help-block\"> First Name Required </p>";
    }
    if ($isformvalid && empty($formdata["lastname"]))
    {
        $isformvalid = false;
        $formdata["errlastname"] = "<p id=\"errlastname\" class=\"help-block\"> Last Name Required </p>";
    }
    if ($isformvalid && empty($formdata["password"]))
    {
        $isformvalid = false;
        $formdata["errpassword"] = "<p id=\"errpassword\" class=\"help-block\"> Password Required </p>";
    }
    if ($isformvalid && empty($formdata["confirmpassword"]))
    {
        $isformvalid = false;
        $formdata["errconfpassword"] = "<p id=\"errconfpassword\" class=\"help-block\"> Please confirm your password </p>";
    }
    if ($isformvalid && $formdata["confirmpassword"] != $formdata["password"])
    {
        $isformvalid = false;
        $formdata["erridenticalpass"] = "<p id=\"erridenticalpass\" class=\"help-block\"> Password is not identical in both fields </p>";
    }
    if ($isformvalid && empty($formdata["email"]))
    {
        $isformvalid = false;
        $formdata["erremail"] = "<p id=\"erremail\" class=\"help-block\"> E-mail Required </p>";
    }
    if($isformvalid)
    {
        $formdata["valid"] = true;
    }
    return $formdata;
}


//check to see if the article previews loaded properly
$pagecontent = createPage($formmethod, $formaction, $formdata);
$tabtitle = "Sign Up";

//build html
$page = new MasterPage($tabtitle);
$page->setDynamicContent2($pagecontent);
$page->renderPage();

?>